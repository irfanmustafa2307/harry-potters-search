const charactersList = document.getElementById('charactersList');
const searchBar = document.getElementById('searchBar');
let hpCharacters = [];

searchBar.addEventListener('keyup', (e) => {
    const searchString = e.target.value.toLowerCase();

    const filteredCharacters = hpCharacters.filter((character) => {
        return (
            character.name.toLowerCase().includes(searchString) ||
            character.house.toLowerCase().includes(searchString) ||
            character.actor.toLowerCase().includes(searchString) ||
            character.gender.toLowerCase().includes(searchString)
        );
    });
    displayCharacters(filteredCharacters);
});

const loadCharacters = async() => {
    try {
        const res = await fetch('https://hp-api.herokuapp.com/api/characters');
        hpCharacters = await res.json();
        displayCharacters(hpCharacters);
        console.log();
    } catch (err) {
        console.error(err);
    }
    console.log(hpCharacters);
};

const displayCharacters = (characters) => {
    const htmlString = characters
        .map((character) => {
            return `
           


        <div class="card mb-3 card-style" style=" ">
            <div class="row g-0">
                <div class="col-md-4">
                    <img class="card-img-top" src="${character.image}" alt="...">
                </div>
                  <div class="col-md-8">
                    <div class="card-body" style="">
                      <h5 class="card-title">${character.name}</h5>
                      <p class="card-text">
                      House : ${character.house}<br>
                      Gender         : ${character.gender}<br>
                      Date Of Birth : ${character.dateOfBirth} <br>
                      </p>
                      <p class="card-text"><small class="text-muted">Actor : ${character.actor}</small></p>
                    </div>
                  </div>
            </div>
        </div>
            `;
        })
        .join('');
    charactersList.innerHTML = htmlString;
};

loadCharacters();